package in3k8.util.bitbucket.restclient

import groovy.json.JsonSlurper

class RestResponse {

   private final int status
   private final String body

   private final JsonSlurper slurper = new JsonSlurper()

   RestResponse(int status,String body) {
       this.status = status
       this.body = body
   }

   Map getBodyAsJsonMap() {
       return slurper.parseText(body) as Map
   }

}
