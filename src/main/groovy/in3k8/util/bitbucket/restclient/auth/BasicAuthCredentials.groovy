package in3k8.util.bitbucket.restclient.auth

import org.apache.http.Header
import org.apache.http.message.BasicHeader
import org.apache.commons.codec.binary.Base64

class BasicAuthCredentials {
    private final String userName
    private final String password

    private final Base64 encoder = new Base64()

    BasicAuthCredentials(String userName, String password) {
        this.userName = userName
        this.password = password

    }


    Header getAuthHeader() {
        return new BasicHeader('Authorization', 'Basic '+encoder.encodeAsString("${userName}:${password}".toString().bytes))
    }
}
