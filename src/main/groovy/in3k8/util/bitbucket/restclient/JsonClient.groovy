package in3k8.util.bitbucket.restclient

import in3k8.util.bitbucket.restclient.auth.BasicAuthCredentials
import org.apache.http.Header
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.utils.URIBuilder
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.BasicHeader

class JsonClient {

    String baseUrl
    BasicAuthCredentials credentials
    private List<Header> defaultHeaders = [new BasicHeader('accept','application/json')]
    HttpClient client

    JsonClient(String baseUrl, BasicAuthCredentials credentials) {
        this.baseUrl = baseUrl
        this.credentials = credentials

        client = new DefaultHttpClient()
    }

    RestResponse get(String path, Map<String,String> params, List<Header> headers = []) {

        URIBuilder builder = new URIBuilder("${baseUrl}${path}")
        appendUrlParameters(builder,params)

        HttpGet httpGet = new HttpGet(builder.build())
        applyHeaders(headers, httpGet)

        HttpResponse response = client.execute(httpGet)

        return new RestResponse(response.statusLine.statusCode, response.entity.content.text)

    }

    private def applyHeaders(List<Header> headers, HttpGet httpGet) {

        defaultHeaders.each { Header header ->
            httpGet.addHeader(header)
        }

        headers.each { Header header ->
            httpGet.addHeader(header)
        }

        if (credentials) {
            httpGet.addHeader(credentials.authHeader)
        }
    }

    private void appendUrlParameters(URIBuilder builder,Map<String,String> validQueryParams) {
        validQueryParams.each {key, value ->
            println "${key} -> ${value}"
            builder.addParameter(key, value)
        }
    }

}

