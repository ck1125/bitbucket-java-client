package in3k8.util.bitbucket.services

import in3k8.util.bitbucket.restclient.JsonClient
import in3k8.util.bitbucket.model.Issue
import in3k8.util.bitbucket.restclient.RestResponse
import in3k8.util.bitbucket.restclient.auth.BasicAuthCredentials
import java.text.SimpleDateFormat
import static java.util.TimeZone.getTimeZone

class BitbucketService {

    SimpleDateFormat UTC_DATE_FORMAT = new SimpleDateFormat('yyyy-MM-dd HH:mm:ss')


    private final String userName
    protected JsonClient bitbucketApiClient

    private List<String> ISSUE_QUERY_KEYS = ['content', 'status', 'version','kind','limit','start']

    BitbucketService(String userName, String password) {
        this.userName = userName
        bitbucketApiClient = new JsonClient('https://api.bitbucket.org/1.0/repositories',
                                            new BasicAuthCredentials(userName,password))

        UTC_DATE_FORMAT.setTimeZone(getTimeZone('UTC'))

    }

    List<Issue> findIssues(String repoId, Map<String,String> params) {
        Map validQueryParams = validateIssueQueryParams(params)

        RestResponse response = bitbucketApiClient.get("/${userName}/${repoId}/issues",validQueryParams)
        Map issueResponse = response.bodyAsJsonMap
        List<Issue> issues = parseIssuesResponse(issueResponse)

        while(issues.size() < issueResponse.count) {
            response = bitbucketApiClient.get("/${userName}/${repoId}/issues",validQueryParams.plus([start:"${issues.size()}"]))
            issueResponse = response.bodyAsJsonMap
            issues.addAll(parseIssuesResponse(issueResponse))
        }

        return sortById(issues)

    }

    private List<Issue> sortById(List<Issue> issues) {
        return issues.sort {Issue a, Issue b ->
            return a.id.compareTo(b.id)
        }
    }

    private List<Issue> parseIssuesResponse(Map json) {

        List<Issue> issues = []
        if (json) {
            issues = json.issues.collect { Map issue ->
                new Issue(
                        id: issue.local_id, status: issue.status, description: issue.content,
                        assignee: issue.responsible?.display_name, reporter: issue.reported_by?.display_name,
                        version: issue.metadata?.version, title: issue.title,
                        lastUpdateTime: parseDateFromUTCString(issue.utc_last_updated))
            }

        }

        return issues
    }

    private Date parseDateFromUTCString(String utcDateString) {
        return UTC_DATE_FORMAT.parse(utcDateString)
    }

    private Map validateIssueQueryParams(Map params) {
        return params.findAll { key, value ->
            ISSUE_QUERY_KEYS.contains(key)
        }
    }


}
