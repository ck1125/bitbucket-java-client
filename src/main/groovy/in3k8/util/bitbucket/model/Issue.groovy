package in3k8.util.bitbucket.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString


@EqualsAndHashCode(excludes='lastUpdateTime')
@ToString
class Issue {
    int id
    String title
    String description
    String version
    String status
    String reporter
    String assignee
    Date lastUpdateTime
}
