package in3k8.util.bitbucket.services;


import in3k8.util.bitbucket.restclient.JsonClient
import in3k8.util.bitbucket.model.Issue
import in3k8.util.bitbucket.restclient.StubRestResponse
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
public class BitbucketServiceSpec extends Specification {

    BitbucketService bitbucketService
    JsonClient mockBitbucketClient

    Map issue1 = [
            local_id: 21,
            status: 'new',
            title: 'blah title 1',
            content: 'blah description',
            utc_last_updated: '2013-03-12 23:29:49+00:00',
            reported_by: [
                    display_name: 'user 1'
            ]

    ]
    Map issue2 = [
            local_id: 1,
            status: 'new',
            title: 'blah title 2',
            content: 'description 2',
            utc_last_updated: '2013-03-11 23:29:49+00:00',
            reported_by: [
                    display_name: 'user 1'
            ],
            responsible: [
                    display_name: 'user 2'
            ],
            metadata: [
                    version: 'version x'
            ]
    ]

    Map issue3 = [
            local_id: 31,
            status: 'new',
            title: 'blah title 3',
            content: 'blah description 3',
            utc_last_updated: '2013-03-13 23:29:49+00:00',
            reported_by: [
                    display_name: 'user 1'
            ]

    ]


    void setup() {
        bitbucketService = new BitbucketService('mockuser','mockpassword')

        mockBitbucketClient = Mock(JsonClient)
        bitbucketService.bitbucketApiClient = mockBitbucketClient
    }

    def 'find issues no query params'() {
        given:
            StubRestResponse restResponse = new StubRestResponse(200, [count:0])
        when:
            List<Issue> issues = bitbucketService.findIssues('repoId',[:])
        then:
            1 * mockBitbucketClient.get('/mockuser/repoId/issues',[:]) >> restResponse
            0 * _
        and:
            issues == []

    }

    def 'find issues ignore unknown params'() {
        given:
            StubRestResponse response = new StubRestResponse(200, '{"count": 0}')

        when:
            List<Issue> issues = bitbucketService.findIssues('repoId',params)
        then:
            1 * mockBitbucketClient.get('/mockuser/repoId/issues', expectedQueryParams) >> response
            0 * _
        and:
            issues == []

        where:
            params                             | expectedQueryParams
            [content:'content']                | [content:'content']
            [content:'content',unknown:'123']  | [content:'content']
            [status:'blah',unknown:'123']      | [status:'blah']
            [version:'blah',content:'123']     | [version:'blah',content:'123']

    }

    def 'find issues loads as many times as needed for page size 1'() {
        given:
            Calendar calendarInstance = Calendar.getInstance()
            calendarInstance.set(2013,2,12,23,29,49)
            Date issue1LUD = calendarInstance.getTime()

            calendarInstance.set(2013,2,11,23,29,49)
            Date issue2LUD = calendarInstance.getTime()

        and:
            StubRestResponse response = new StubRestResponse(200, [count : 2, issues:[issue1]])
            StubRestResponse response2 = new StubRestResponse(200, [count : 1, issues:[issue2]])

        when:
            List<Issue> issues = bitbucketService.findIssues('repoId',[:])
        then:
            1 * mockBitbucketClient.get('/mockuser/repoId/issues', [:]) >> response
            1 * mockBitbucketClient.get('/mockuser/repoId/issues', [start:'1']) >> response2
            0 * _
        and:
            issues == [
                    new Issue(id: 1,status: 'new',title: 'blah title 2',description: 'description 2', reporter: 'user 1', assignee: 'user 2', version: 'version x', lastUpdateTime: issue2LUD),
                    new Issue(id: 21,status: 'new',title: 'blah title 1',description: 'blah description', reporter: 'user 1',lastUpdateTime: issue1LUD ),
            ]

    }
    def 'find issues loads as many times as needed for page size 2'() {
        given:
            Calendar calendarInstance = Calendar.getInstance()
            calendarInstance.set(2013,2,12,23,29,49)
            Date issue1LUD = calendarInstance.getTime()

            calendarInstance.set(2013,2,11,23,29,49)
            Date issue2LUD = calendarInstance.getTime()

            calendarInstance.set(2013,2,13,23,29,49)
            Date issue3LUD = calendarInstance.getTime()

        and:
            StubRestResponse response = new StubRestResponse(200, [count : 3, issues:[issue1,issue2]])
            StubRestResponse response2 = new StubRestResponse(200, [count : 1, issues:[issue3]])

        when:
            List<Issue> issues = bitbucketService.findIssues('repoId',[:])
        then:
            1 * mockBitbucketClient.get('/mockuser/repoId/issues', [:]) >> response
            1 * mockBitbucketClient.get('/mockuser/repoId/issues', [start:'2']) >> response2
            0 * _
        and:
            issues == [
                    new Issue(id: 1,status: 'new',title: 'blah title 2',description: 'description 2', reporter: 'user 1', assignee: 'user 2', version: 'version x', lastUpdateTime: issue2LUD),
                    new Issue(id: 21,status: 'new',title: 'blah title 1',description: 'blah description', reporter: 'user 1',lastUpdateTime: issue1LUD ),
                    new Issue(id: 31,status: 'new',title: 'blah title 3',description: 'blah description 3', reporter: 'user 1', lastUpdateTime: issue3LUD)
            ]

    }

    def 'find issues returns data'() {
        given:
            Calendar calendarInstance = Calendar.getInstance()
            calendarInstance.set(2013,2,12,23,29,49)
            Date issue1LUD = calendarInstance.getTime()

            calendarInstance.set(2013,2,11,23,29,49)
            Date issue2LUD = calendarInstance.getTime()

        StubRestResponse response = new StubRestResponse(200, [issues: [issue1,issue2]])
        when:
            List<Issue> issues = bitbucketService.findIssues('repoId',[:])
        then:
            1 * mockBitbucketClient.get('/mockuser/repoId/issues',[:]) >> response
            0 * _

        and:
            issues == [
                    new Issue(id: 1,status: 'new',title: 'blah title 2',description: 'description 2', reporter: 'user 1', assignee: 'user 2', version: 'version x', lastUpdateTime: issue2LUD),
                    new Issue(id: 21,status: 'new',title: 'blah title 1',description: 'blah description', reporter: 'user 1',lastUpdateTime: issue1LUD ),
            ]

    }

}
