package in3k8.util.bitbucket.restclient.auth;


import org.apache.http.Header
import spock.lang.Specification

public class BasicAuthCredentialsSpec extends Specification {

    BasicAuthCredentials credentials

    def 'get auth header'() {
        given:
            credentials = new BasicAuthCredentials('uname','pass')
        when:
            Header authHeader = credentials.getAuthHeader()
        then:
            authHeader.name == 'Authorization'
            authHeader.value.startsWith('Basic ')
            new String(authHeader.value.substring(6).decodeBase64()) == 'uname:pass'
    }
}
