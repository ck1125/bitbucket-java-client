package in3k8.util.bitbucket.restclient

class StubRestResponse extends RestResponse {

    Map mapBody

    StubRestResponse(int status, String body) {
        super(status,body)
    }

    StubRestResponse(int status, Map body) {
        super(status,null)
        this.mapBody = body
    }

    @Override
    Map getBodyAsJsonMap() {
        return (mapBody) ? mapBody : super.bodyAsJsonMap
    }


}
